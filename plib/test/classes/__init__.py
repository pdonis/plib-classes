#!/usr/bin/env python
"""
Sub-Package TEST.CLASSES of Package PLIB
Copyright (C) 2008-2015 by Peter A. Donis

Released under the GNU General Public License, Version 2
See the LICENSE and README files for more information

This sub-package contains the PLIB.CLASSES test suite.
"""
